function buscarPorId() {
    const inputId = document.getElementById('inputId').value;
    let res = document.getElementById('lista');
    res.innerHTML = ''; // Limpiar la tabla antes de mostrar los resultados de la búsqueda

    if (inputId.trim() !== '') {
        const http = new XMLHttpRequest();
        const url = "https://jsonplaceholder.typicode.com/albums/" + inputId;

        http.onreadystatechange = function () {
            if (this.status == 200 && this.readyState == 4) {
                const datos = JSON.parse(this.responseText);
                res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>'
                    + '<td class="columna2">' + datos.id + '</td>'
                    + '<td class="columna3">' + datos.title + '</td> </tr>';
            }
        };

        http.open('GET', url, true);
        http.send();
    }
}
document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
    let input = document.getElementById('inputId');
});


document.getElementById("btnBuscar").addEventListener('click', buscarPorId);