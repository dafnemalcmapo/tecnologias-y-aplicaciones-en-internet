document.getElementById("btnBuscar").addEventListener("click", () => {
    let userId = document.getElementById('inputId').value;
    const url = `https://jsonplaceholder.typicode.com/users/${userId}`;
    
    axios.get(url)
        .then(response => {
            const user = response.data;
            updateFields(user);
        })
        .catch(error => {
            console.error("Error al realizar la petición:", error);
        });
});

function updateFields(user) {
    document.getElementById('name').value = user.name;
    document.getElementById('UserName').value = user.username;
    document.getElementById('Email').value = user.email;
    document.getElementById('street').value = user.address.street;
    document.getElementById('number').value = user.address.suite;
    document.getElementById('city').value = user.address.city;
}


document.getElementById("btnLimpiar").addEventListener('click', function () {
    let nameInput = document.getElementById('name');
    let UserNameInput = document.getElementById('UserName');
    let EmailInput = document.getElementById('Email');
    let streetInput = document.getElementById('street');
    let numberInput = document.getElementById('number');
    let cityInput = document.getElementById('city');

    nameInput.value = "";
    UserNameInput.value = "";
    EmailInput.value = "";
    streetInput.value = "";
    numberInput.value = "";
    cityInput.value = "";
});
