document.addEventListener("DOMContentLoaded", function () {
    document.getElementById("btnCargar").addEventListener("click", function () {
        cargarRazas();
    });

    document.getElementById("btnVer").addEventListener("click", function () {
        verImagen();
    });
});

function cargarRazas() {
    axios.get("https://dog.ceo/api/breeds/list")
        .then(response => {
            document.getElementById("raza").innerHTML = "";  // Vaciar el select

            document.getElementById("raza").add(new Option('Selecciona una raza', ''));

            response.data.message.forEach(raza => {
                let option = new Option(raza, raza);
                document.getElementById("raza").add(option);
            });
        })
        .catch(error => {
            console.error("Error al cargar las razas:", error);
        });
}

function verImagen() {
    let razaSeleccionada = document.getElementById("raza").value;

    if (razaSeleccionada) {
        let urlImagen = `https://dog.ceo/api/breed/${razaSeleccionada}/images/random`;

        axios.get(urlImagen)
            .then(response => {
                document.getElementById("img").src = response.data.message;
            })
            .catch(error => {
                console.error("Error al cargar la imagen:", error);
            });
    } else {
        alert("Selecciona una raza antes de ver la imagen.");
    }
}
